const express = require('express');
const path = require('path');
const fs = require('fs');
const multer = require('multer');
const Item = require('../models/Item');

const router = express.Router();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
})
 
const upload = multer({ storage: storage })
router.post("/", upload.single('myImage'), (req,res) => {
	let newItem = new Item(req.body);
	newItem.image = req.file.filename;
	newItem.save()
	.then(data => {
		data.populate("category").execPopulate()
		.then(datanew => res.send(datanew));
	});
})

router.get('/', (req, res) => {
	Item.find({}).populate("category")
	.then(data => {
		res.send(data)
	});	
})

router.get('/:id', (req, res) => {
	Item.findById(req.params.id).populate("category")
	.then(data => {
		res.send(data)
	});	
})

router.delete('/:id', (req, res) => {
	Item.findOneAndDelete({_id: req.params.id}, { useFindAndModify: false }) 
	.then(data => {
		fs.unlinkSync("./public/uploads/"+data.image);
		res.send(data);
	});
})

router.put("/:id", upload.single('myImage'), (req, res) => {
	let item = req.body;
	if(req.file) item.image = req.file.filename;
	Item.findOneAndUpdate({_id: req.params.id}, req.body, { new: true, useFindAndModify: false })
	.populate("category")
	.then(data => {
		res.send(data);
	})
})

module.exports = router;