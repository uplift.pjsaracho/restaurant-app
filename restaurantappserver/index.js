const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const categoryRouter = require('./routers/categoryRouter');
const itemRouter = require('./routers/itemRouter');
const userRouter = require('./routers/userRouter');

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/restaurantapp', { useNewUrlParser: true,  useUnifiedTopology: true });

mongoose.set('useCreateIndex', true);

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static('public/uploads'));

app.use('/users', userRouter);
app.use('/categories', categoryRouter);
app.use('/items', itemRouter);

app.listen(8080, () => console.log("running on port 8080"));