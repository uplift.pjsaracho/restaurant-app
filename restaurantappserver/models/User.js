const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = mongoose.Schema({
	username: {
		type: String,
		unique: true
	},
	password: String
});

module.exports = mongoose.model('User', UserSchema);
