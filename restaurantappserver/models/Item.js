const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ItemSchema = new Schema({
	name: String,
	price: Number,
	category: { 
		type: Schema.Types.ObjectId, ref: 'Category' 
	},
	image: String
});

module.exports = mongoose.model('Item', ItemSchema);
