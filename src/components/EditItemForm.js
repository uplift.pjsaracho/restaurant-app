import React from 'react';
import axios from 'axios';

class EditItemForm extends React.Component {
	state = {
		categories: [],
		image: null,
		item: {
			name: "",
			price: 0,
			category: "",
			image: undefined
		}
	}

	componentDidMount = async() => {
		const catRes = await axios.get("http://localhost:8080/categories")
		const itemRes = await axios.get("http://localhost:8080/items/"+this.props.match.params.id)
		this.setState({
			categories: catRes.data,
			item: itemRes.data
		})
	}

	editItemBtnClickHandler = () => {
		const formData = new FormData();
		formData.append('name', this.state.item.name);
		formData.append('price', this.state.item.price);
		formData.append('category', this.state.item.category._id);
		formData.append('myImage',this.state.image);
		const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        axios.put("http://localhost:8080/items/"+this.state.item._id, formData, config)
        .then(res => {
			this.props.editItem(res.data);
			this.props.history.push("/");
        })
	}

	render(){
		return(
			<form>
				<h2>Edit Item</h2>
				Name: <input 
					value={this.state.item.name} 
					onChange={e=>this.setState({ item: {...this.state.item, name : e.target.value}} )} 
					type="text" 
				/><br/>
				Price: <input value={this.state.item.price} onChange={e=>this.setState({item: {...this.state.item, price : e.target.value}})} type="number" min="0" /><br/>
				Category: 
				<select value={this.state.item.category._id} onChange={(e)=>this.setState({item: {...this.state.item, category:this.state.categories.find( c => c._id === e.target.value ) }})}>
					{
						this.state.categories.map( c =>
							<option 
								key={c._id} 
								value={c._id}
							>
								{c.name}
							</option>
						)
					}
				</select><br/>
				<img alt="item" src={this.state.item.image ? "http://localhost:8080/"+this.state.item.image : ""} width="200px"/><br/>
				Image <input type="file" onChange={e=>this.setState({image: e.target.files[0]})} /><br/>
				<button type="button" onClick={this.editItemBtnClickHandler}>Edit Item</button>
			</form>
		);
	}
}

export default EditItemForm;
