import React from 'react';
import Item from './Item';
import { Link } from 'react-router-dom';

const Items = props => {
	const items = props.match.params.category ? 
		props.items
		.filter( i => i.category.name.toLowerCase() === props.match.params.category)
		:
		props.items
		;

	return(
		<React.Fragment>
			<div className="filterCategory">
				<Link to="/menu">All</Link>
				{
				  props.categories.map( c => 
				    <Link key={c._id} to={"/menu/"+c.name.toLowerCase()}>{c.name}</Link>
				  )
				}
			</div>
			<div>
				{
					items
					.map( i => <Item key={i._id} item={i} history={props.history} deleteItem={props.deleteItem} /> )
				}
			</div>
		</React.Fragment>
	)
}

export default Items;