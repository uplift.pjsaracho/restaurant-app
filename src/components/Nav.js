import React from 'react';
import './Nav.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const Nav = props => {
	return(
		<nav className="clearfix">
			<ul className="left">
				<li><Link to="/">Home</Link></li> |
				<li><Link to="/admin/add-item">Add Item</Link></li>
			</ul>
			<ul className="right">
				{
					props.isLoggedIn ?
						<li onClick={()=> {props.logout(); localStorage.removeItem('user')}}><Link to="/">Logout</Link></li>
					:	
						<React.Fragment>	
							<li><Link to="/login">Login</Link></li> |
							<li><Link to="/register">Register</Link></li>
						</React.Fragment>	
				}
			</ul>
		</nav>
	);
}

const mapStateToProps = state => {
	return {
		isLoggedIn: state.user.isLoggedIn
	}
}

const mapDispatchToProps = dispatch => {
	return {
		logout: () => dispatch({
			type: 'LOGOUT',
		})
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Nav);