import React from 'react';

const Item = props => {
	return(
		<div key={props.item._id} className="Item">
			<div>
				<img src={props.item.image.substring(0,7) === "myImage" ? "http://localhost:8080/"+props.item.image : props.item.image} alt="menu item" />
			</div>
		  	<p className="itemName">
		  		{props.item.name}
		  		&nbsp;<span className="itemCategory">{props.item.category.name}</span>
		  	</p>
		  	<p className="itemPrice">Php {props.item.price.toFixed(2)}</p>
		  	<button className="edit" onClick={()=>props.history.push("/admin/edit-item/"+props.item._id)}>Edit</button>
		  	<button className="delete" onClick={()=>props.deleteItem(props.item._id)}>Delete</button>
		</div>
	);
}

export default Item;