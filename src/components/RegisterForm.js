import React from 'react';
import axios from 'axios';

class RegisterForm extends React.Component {
	state = {
		username: "",
		password: "",
		cpassword: ""
	}

	formSubmitHandler = (e) => {
		e.preventDefault();

		axios.post("http://localhost:8080/users", {username:this.state.username, password: this.state.password})
		.then(res => {
			console.log(res.data);
		});
	}

	render() {
		return(
			<form onSubmit={this.formSubmitHandler}>
				Username: 
				<input 
					type="text" 
					value={this.state.username}
					onChange={e=>this.setState({username: e.target.value})}
				/><br/>
				Password: 
				<input type="password" 
					value={this.state.password}
					onChange={e=>this.setState({password: e.target.value})}
				/><br/>
				Confirm Password: 
				<input 
					type="password" 
					value={this.state.cpassword}
					onChange={e=>this.setState({cpassword: e.target.value})}
				/><br/>
				<button>Register</button>
			</form>
		);
	}
}

export default RegisterForm;