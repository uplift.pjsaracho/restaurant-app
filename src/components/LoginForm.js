import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

class LoginForm extends React.Component {
	state = {
		username: "",
		password: ""
	}

	formSubmitHandler = (e) => {
		e.preventDefault();

		axios.post("http://localhost:8080/users/login", {username:this.state.username, password: this.state.password})
		.then(res => {
			if(res.data) {
				localStorage.setItem("user", JSON.stringify(res.data));
				this.props.login(res.data);
				this.props.history.push('/');
			}
		});

	}

	render(){
		return(
			<form onSubmit={this.formSubmitHandler}>
				Username: 
				<input 
					type="text" 
					value={this.state.username}
					onChange={e=>this.setState({username: e.target.value})}
				/><br/>
				Password: 
				<input type="password" 
					value={this.state.password}
					onChange={e=>this.setState({password: e.target.value})}
				/><br/>
				<button>Login</button>
			</form>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		login: user => dispatch({
			type: 'LOGIN',
			payload: user,
		})
	}
}

export default connect(null, mapDispatchToProps)(LoginForm);