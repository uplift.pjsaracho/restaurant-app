import React from 'react';
import axios from 'axios';

class AddItemForm extends React.Component {
	state = {
		categories: [],
		image: null,
		newItem: {
			name: "",
			price: 0,
			category: "",
			image: "https://image.flaticon.com/icons/svg/1046/1046747.svg"
		}
	}

	componentDidMount = () => {
		axios.get("http://localhost:8080/categories")
		.then( res => {
			this.setState({
				categories: res.data,
				newItem: {
					...this.state.newItem,
					category: res.data[0]
				}
			});
		})
	}

	addItemBtnClickHandler = () => {
		const formData = new FormData();
		formData.append('name', this.state.newItem.name);
		formData.append('price', this.state.newItem.price);
		formData.append('category', this.state.newItem.category._id);
		formData.append('myImage',this.state.image);
		const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        axios.post("http://localhost:8080/items", formData, config)
        .then(res => {
			this.props.addItem(res.data);
			this.props.history.push("/");
        })
	}

	render(){
		console.log(this.state.newItem)
		return(
			<form>
				Name: <input 
					value={this.state.newItem.name} 
					onChange={e=>this.setState({ newItem: {...this.state.newItem, name : e.target.value}} )} 
					type="text" 
				/><br/>
				Price: <input value={this.state.newItem.price} onChange={e=>this.setState({newItem: {...this.state.newItem, price : e.target.value}})} type="number" min="0" /><br/>
				Category: 
				<select onChange={(e)=>this.setState({newItem: {...this.state.newItem, category:this.state.categories.find( c => c._id === e.target.value ) }})}>
					{
						this.state.categories.map( c => 
							<option key={c._id} value={c._id}>{c.name}</option>
						)
					}
				</select><br/>
				Image <input type="file" onChange={e=>this.setState({image: e.target.files[0]})} />
				<button type="button" onClick={this.addItemBtnClickHandler}>Add Item</button>
			</form>
		);
	}
}

export default AddItemForm;
