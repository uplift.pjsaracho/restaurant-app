import React from 'react';
import axios from 'axios';

class AddItemForm2 extends React.Component {
	state = {
		name: "",
		price: 0,
		category: "",
		categories: []
	}

	componentDidMount = () => {
		axios.get("http://localhost:8080/categories")
		.then(res => {
			this.setState({
				category: res.data[0].name,
				categories : res.data
			})
		})
	}

	addBtnClickHandler = () => {
		let newItem = {
			name: this.state.name,
			price: this.state.price,
			category: this.state.category
		}
		axios.post("http://localhost:8080/items", newItem)
		.then(res => {
			this.props.addItem(res.data);
		})
	}

	render() {
		return(
			<form>
				<h2>Add Item</h2>
				Name 
				<input 
					type="text" 
					value={this.state.name}
					onChange={e => this.setState({name: e.target.value})}
				/><br/>
				Price 
				<input 
					type="number"
					value={this.state.price}
					onChange={e => this.setState({price: e.target.value})} 
				/><br/>
				Category 
				<select onChange={e=>this.setState({category: e.target.value})}>
					{
						this.state.categories.map( c => <option key={c._id}>{c.name}</option>)
					}
				</select><br />
				<button onClick={this.addBtnClickHandler} type="button">Add</button>
			</form>
		);
	}
}

export default AddItemForm2;