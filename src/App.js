import React from 'react';
import axios from 'axios';
import './App.css';
import { Route } from 'react-router-dom';

import Nav from './components/Nav';
import RegisterForm from './components/RegisterForm';
import LoginForm from './components/LoginForm';
import AddItemForm from './components/AddItemForm';
import EditItemForm from './components/EditItemForm';
import Items from './components/Items';


class App extends React.Component {
  state = {
    categories: [],
    items: []
  }

  componentDidMount = async() => {
      const itemsRes = await axios.get("http://localhost:8080/items")
      // .then(res => {
      //   this.setState({items: res.data})
      // })
      const items = itemsRes.data;

      const categoriesRes = await axios.get("http://localhost:8080/categories")
      // .then(res => {
      //   this.setState({categories: res.data})
      // })
      const categories = categoriesRes.data;

      this.setState({
        items: items,
        categories: categories
      })
  }

  addItemHandler = (newItem) => {
    let items = JSON.parse(JSON.stringify(this.state.items));
    items.push(newItem);
    this.setState({items: items});
  }

  editItemHandler = (item) => {
    let items = JSON.parse(JSON.stringify(this.state.items));
    items = items.map( i => {
      if(i._id === item._id) {
        i = item;
      }
      return i;
    })
    this.setState({
      items: items
    });
  }

  deleteItemHandler = (id) => {
    axios.delete("http://localhost:8080/items/"+id)
    .then(res => {
      let items = JSON.parse(JSON.stringify(this.state.items));
      items = items.filter( i => i._id !== id);
      this.setState({items: items});
    })
  }

  render() { 
    return (
      <React.Fragment>
        <Nav />
        <div className="App">
          <Route path="/register" component={RegisterForm} />
          <Route path="/login" render={props => <LoginForm {...props} />} />
          <Route path="/admin/add-item" render={props => <AddItemForm {...props} addItem={this.addItemHandler} />} />
          <Route path="/admin/edit-item/:id" render={props=> <EditItemForm {...props} editItem={this.editItemHandler} />} />
          <Route exact path="(/|/menu)" render={props=> <Items {...props} categories={this.state.categories} items={this.state.items} deleteItem={this.deleteItemHandler}/>} />
          <Route path="/menu/:category" render={props=> <Items {...props} categories={this.state.categories} items={this.state.items} deleteItem={this.deleteItemHandler} />} />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
