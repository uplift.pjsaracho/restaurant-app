# Restaurant App

> A full-stack app created using the MERN stack.  

## Build Setup

This repo contains a node-express server connecting to a local instance of mongodb running on port 27017, and a React app.    

To use a sample set of mongodb data, we have a dump folder inside restaurantappserver:

``` bash
# Move into the server directory
cd restaurantappsserver

# Import data
mongorestore
``` 

To run the server:  

``` bash
# Move into the server directory
cd restaurantappserver

# install dependencies
npm install

# serve
node index.js  

Note: Make sure mongodb is running, with a 'restaurantapp' database. DB Collections: users, items, categories. 
```

To run the React app:

``` bash
# install dependencies
npm install

# serve
npm start
```

For detailed explanation on how things work, consult the  
>[official React documentation](https://reactjs.org/docs/getting-started.html)  
>[official Express.js documentation](https://expressjs.com)  
>[official MongoDB documentation](https://docs.mongodb.com/manual)  

## Contact Information

Feel free to reach out to and connect with the instructor for any inquiries or problems with the sample project.

**PeeJay Saracho**  
0915 980 1701  
uplift.pjsaracho@gmail.com  
